<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Service;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->session()->regenerate();
        return view('home');
    }

    /**
     * Get Transactions from the endpoint.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTransactions(Request $request)
    {
        $aValidation = $this->validateRequest($request);
        if( !$aValidation["result"] )
            return $aValidation;

        $oService = new Service;
        $aTransactionList = $oService->fetchTransactions($request->all());
        $aReport = $oService->fetchReport($request->all());

        if( $aReport["result"] && $aTransactionList["result"] )
        {
            return [
                'result' => true,
                'report' => $aReport,
                'transactions' => $aTransactionList
            ];
        }

        return [
            'result' => false,
            'error' => $aReport["error"] ?? $aTransactionList["error"],
        ];
    }

    public function getMoreTransactions(Request $request)
    {
        $aValidation = $this->validateRequest($request);
        if( !$aValidation["result"] )
            return $aValidation;

        $oService = new Service;
        $aTransactionList = $oService->fetchTransactions($request->all());
        $aReport = $oService->fetchReport($request->all());

        return $aTransactionList;
    }

    /**
     * Get Status from the endpoint.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStatus(Request $request)
    {
        $aValidation = $this->validateRequest($request);
        if( !$aValidation["result"] )
            return $aValidation;

        $oService = new Service;
        return $oService->fetchStatus($request->all());
    }

    /**
     * Validate form inputs.
     *
     * @return \Illuminate\Http\Response
     */
    private function validateRequest($request)
    {
        $validator = Validator::make($request->all(), [
            'start' => 'required|date_format:Y-m-d',
            'end' => 'required|date_format:Y-m-d',
            'page' => 'integer|min:1'
        ]);

        if ( $validator->fails() || (strtotime($request->start) > strtotime($request->end)) ) {
            return [
                'result' => false,
                'error' => 'Bad inputs'
            ];
        }
        return [
            'result' => true
        ];
    }
}
