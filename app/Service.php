<?php

namespace App;

use GuzzleHttp\Client;

class Service
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * Get or Refresh Token depending on TOKEN_LIFETIME.
     *
     * @return bearer token
     */

    private function getToken () {

        // if token is stored into session and it's not expired
        if (session()->has('Bearer') 
            && session()->get('tokenExpireTime') > (strtotime(date("Y-m-d H:i:s")) - env('API_TOKEN_LIFETIME')) )
        {
            return $this->success(session()->get('Bearer'));
        }


        session()->forget('Bearer');
        $requestTime = strtotime(date("Y-m-d H:i:s"));

        try
        {
            $response = $this->client->post(env('API_BASE_URL') . env('API_LOGIN_URL'), [
                'form_params' => [
                    'email' => env('API_USERNAME'),
                    'password' => env('API_PASSWORD')
                ]
            ]);
        }
        catch(\Exception $e) {
            return $this->error( $e->getMessage() );
        }

        $result = json_decode($response->getBody()->getContents(), true);

        if( $result["status"] != env('API_TOKEN_SUCCESS') ) {
            return $this->error( $result["message"] ?? 'General Error' );
        }

        session()->put('Bearer', $result["token"]);
        session()->put('tokenExpireTime', $requestTime);

        return $this->success( $result["token"] );
    }

    /**
     * fetch transactions from the service
     *
     * @return error
     */

    public function fetchTransactions( $params ) 
    {        
        $aToken = $this->getToken();

        if ( !$aToken["result"] )
            return $aToken;

        // fetching from API
        try
        {
            $response = $this->client->post(env('API_BASE_URL') . env('API_TRANSACTIONS_URL'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $aToken["data"]
                ],
                'json' => [
                    'fromDate' => $params["start"],
                    'toDate' => $params["end"],
                    'page' => $params["page"] ?? 1
                ]
            ]);
        }
        catch(\Exception $e) {
            return $this->error( $e->getMessage() );
        }

        $aTransactionList = json_decode($response->getBody()->getContents(), true);

        if( !empty($aTransactionList["data"] ))
            $aTransactionList["headers"] = array_keys($aTransactionList["data"][0]);

        return $this->success($aTransactionList);
    }

    /**
     * fetch report from the service
     *
     * @return error
     */

    public function fetchReport( $params ) 
    {        
        $aToken = $this->getToken();

        if ( !$aToken["result"] )
            return $aToken;

        // fetching from API
        try
        {
            $response = $this->client->post(env('API_BASE_URL') . env('API_REPORT_URL'), [
                'headers' => [
                    'Content-Type' => 'application/json',
                    'Authorization' => $aToken["data"]
                ],
                'json' => [
                    'fromDate' => $params["start"],
                    'toDate' => $params["end"]
                ]
            ]);
        }
        catch(\Exception $e) {
            return $this->error( $e->getMessage() );
        }

        $aReportList = json_decode($response->getBody()->getContents(), true);

        if( $aReportList["status"] != env('API_TOKEN_SUCCESS') ) {
            return $this->error( $aReportList["message"] ?? 'General Error' );
        }

        return $this->success($aReportList["response"]);
    }

    /**
     * Return error response
     *
     * @return error
     */

    private function error( $msg ) {
        return [
            'result' => false,
            'error' => $msg
        ];
    }

    /**
     * Return success response
     *
     * @return success
     */

    private function success( $data ) {
        return [
            'result' => true,
            'data' => $data
        ];
    }
}
